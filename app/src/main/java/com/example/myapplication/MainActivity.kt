package com.example.myapplication

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth


class MainActivity : AppCompatActivity() {
    private lateinit var passDontMatch: TextView
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword : EditText
    private lateinit var editTextRepeatPassword : EditText
    private lateinit var registrationButton : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        init()

        registration()
    }

    private fun init(){
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById((R.id.editTextPassword))
        editTextRepeatPassword = findViewById((R.id.editTextRepeatPassword))
        registrationButton= findViewById(R.id.registrationButton)
        passDontMatch = findViewById(R.id.passDontMatch)
    }
    private fun registration(){

        registrationButton.setOnClickListener{
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val repeatPassword = editTextRepeatPassword.text.toString()

            if (email.isEmpty()|| password.isEmpty() || repeatPassword.isEmpty()){
                Toast.makeText(this, "Please fill the registration form", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password == repeatPassword){
                FirebaseAuth
                    .getInstance()
                    .createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener{task ->
                        if (task.isSuccessful){
                            Toast.makeText(this, "Registration successful", Toast.LENGTH_SHORT).show()
                            editTextPassword.setTextColor(Color.parseColor("#000000"))
                            editTextEmail.setText("")
                            editTextPassword.setText("")
                            editTextRepeatPassword.setText("")
                            passDontMatch.setText("")
                        }else{
                            Toast.makeText(this, "You are already registered", Toast.LENGTH_SHORT).show()
                            editTextPassword.setTextColor(Color.parseColor("#000000"))
                            editTextEmail.setText("")
                            editTextPassword.setText("")
                            editTextRepeatPassword.setText("")
                            passDontMatch.setText("")
                        }
                    }
            }else{
                editTextPassword.setTextColor(Color.parseColor("#FF0000"))
                editTextRepeatPassword.setText("")
                passDontMatch.setText("Passwords Don't match")
            }
        }
    }


}
